//
//  main.cpp
//  Quiz for 9815
//
//  Yanhao Miao
//

#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>

using namespace std;

void swap(int *x, int *y)
{
    int temp = *x;
    *x = *y;
    *y = temp;
}

// Take right as pivot, put all the elements greater than pivot to the right,
// all the elements smaller than pivot to the left
int pivot_move (int arr[], int left, int right)
{
    int pivot = arr[right]; // set the pivot
    int i = left - 1;
  
    for (int j = left; j <= right - 1; j++)
    {
        if (arr[j] < pivot)
        {
            i++; // the next smaller element
            swap(&arr[i], &arr[j]);
        }
    }
    swap(&arr[i + 1], &arr[right]);
    return i + 1;
}
  
// arr is the input array of integers, start/end is the position of
// starting element and ending element
void quickSort(int arr[], int start, int end)
{
    if (start < end)
    {
        int idx = pivot_move(arr, start, end);
        // divide and conquer
        quickSort(arr, start, idx - 1);
        quickSort(arr, idx + 1, end);
    }
}

// a print function
void print(int arr[], int length)
{
    for (int i = 0; i < length; i++)
        cout << arr[i] << " ";
    cout << endl;
}

class Max_heap
{
public:
    // vector to store heap elements
    vector<int> data;
    
    // return parent
    int parent(int i) { return (i - 1) / 2; }
 
    // return left child
    int left_child(int i) { return (2 * i + 1); }
 
    // return right child
    int right_child(int i) { return (2 * i + 2); }
    
    // return the size
    int size() { return data.size(); }

    // if the child of index idx violates the heap property
    void heapify_down(int idx)
    {
        // get left and right child of node at index idx
        int left = left_child(idx);
        int right = right_child(idx);
        int largest = idx;
 
        // find the largest value
        if (left < size() && data[left] > data[idx])
            largest = left;
        if (right < size() && data[right] > data[largest])
            largest = right;
 
        // swap with child having greater value
        if (largest != idx) {
            swap(data[idx], data[largest]);
            heapify_down(largest); // recursive use of heapify_down
        }
    }
 
    // Heapify-up algorithm
    void heapify_up(int idx)
    {
        // check if node at index idx and its parent violates the heap property
        if (idx != 0 && (data[parent(idx)] < data[idx]))
        {
            swap(data[idx], data[parent(idx)]);
            // call Heapify-up on the parent
            heapify_up(parent(idx));
        }
    }
    
    // Add an element to the max_heap
    void Add(int ele)
    {
        data.push_back(ele);
        // get element index and heapify-up
        int index = size() - 1;
        heapify_up(index);
    }
 
    // remove the top element
    void Pop()
    {
        if (size() > 0)
        {
            // put the last element to the root, delete the last
            data[0] = data.back();
            data.pop_back();
            // heapify-down the root node
            heapify_down(0);
        }
        else
            cout<<"Heap is empty!"<<endl;
    }

};
  
int main()
{
    int arr[] = { 9,8,11,-6 };
    quickSort(arr, 0, 3);
    print(arr, 4);
    
    Max_heap mh;
    mh.Add(1);
    mh.Add(5);
    mh.Add(8);
    mh.Add(3);
    mh.Add(9);
    mh.Add(6);
    
    for (int i=0;i<mh.data.size();i++)
        cout<<mh.data[i]<<" ";
    cout<<endl;
    
    return 0;
}
